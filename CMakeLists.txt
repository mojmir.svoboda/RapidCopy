cmake_minimum_required(VERSION 3.10.0)

project(RapidCopy VERSION 1.0.1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

if(CMAKE_VERSION VERSION_LESS "3.7.0")
    set(CMAKE_INCLUDE_CURRENT_DIR ON)
endif()

find_package(Qt5 COMPONENTS Widgets Multimedia REQUIRED)

add_executable(RapidCopy
	main.cpp
	xxhash.cpp
	xxhash.h
	mainwindow.cpp
	cfg.cpp
	fastcopy.cpp
	osl.cpp
	regexp.cpp
	tapi32ex.cpp
	tapi32v.cpp
	tlist.cpp
	tmisc.cpp
	utility.cpp
	version.cpp
	mainsettingdialog.cpp
	confirmdialog.cpp
	aboutdialog.cpp
	finactdialog.cpp
	smtp.cpp
	jobdialog.cpp
	qblowfish.cpp
	joblistrenamedialog.cpp

	cfg.h
	fastcopy.h
	osl.h
	regexp.h
	tapi32ex.h
	tapi32v.h
	tlib.h
	tmisc.h
	utility.h
	version.h
	resource.h
	mainwindow.h
	mainsettingdialog.h
	confirmdialog.h
	aboutdialog.h
	finactdialog.h
	smtp.h
	jobdialog.h
	qblowfish.h
	qblowfish_p.h
	joblistrenamedialog.h

	mainwindow.ui
	mainsettingdialog.ui
	confirmdialog.ui
	aboutdialog.ui
	finactdialog.ui
	jobdialog.ui
	joblistrenamedialog.ui

	res.qrc
)


# qt dependencies
target_link_libraries(RapidCopy Qt5::Widgets Qt5::Multimedia)

# system dependencies
target_link_libraries(RapidCopy acl bsd pthread)

target_include_directories(RapidCopy PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include/RapidCopy>
)

